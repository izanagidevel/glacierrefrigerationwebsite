Hey Sam and Calvin. 

So here's things I've done today that require explanation.
For a complete list of what I've done, look at my commit messages through gitk
gitk is a command in the terminal, just enter it in the root folder.

I organized the files to Website and non-Website.
This was because alot of files that were not needed for the website were in the same root directory.
All of our files for the website are now in the Website/ directory.
I created a Resource/ directory in there to hold all our images (and scripts?),
as of now there is only logo image and the background image.
I was thinking we'd still keep JoannasFiles/, but copy them into Resources/ as necessary.

Also, I've created support for PHP files. 
This means that the general header information, topMenu, and footer can be loaded in,
so if we decide to change it, we don't have to change all the page files. 
You can find that information in their respective php files.
Unfortunately to run PHP files it becomes quite a bit more complicated,
rather than dragging the files into the browser we need to run a local server.
This is bad news for Joanna because it becomes fairly technical for her to view the webpage.
I don't have a solution for that at the moment, 
we could take a screenshot for her before we signoff everytime, I don't know.

But this is because browsers only read html (javascript too maybe?)
PHP needs to be (compiled?) server side to create the html.

Fortunately for us PHP has recently added server support,
so we don't have to download alot of random stuff to run a local server to test our PHP files

Sam's instructions (linux):
	//Everything in between [ ] will be a linux command

	1. Install PHP via 
		[sudo apt-get install php5-cli]
	2. Go to the website folder, so DragonWarriors/Website
	3. Run [php -S localhost:8000] to start your local server
	4. You can now view the files through the browser, go to
		"localhost:8000/index.php" to view the site

	//This will hold up that terminal for PHP error messages etc,
	//so to continue using the terminal you'll need to open up another one.
	//If this doesn't work, try installing apache through
	//[sudo apt-get install apache2-bin] then start at #2

Calvin's instructions (mac):
	//So I haven't actually tried this because I don't have a mac
	//You'll need to do some research if this doesn't work
	//everything in [ ] is a bash command
	//If you're home, you could install VMware now and use the above instructions
	1. Enable Apache, go to system preferences on the dock

	2. Click sharing icon in the Internet & network section
	3. Make sure the "web Sharing" box is enabled
	4. I read Mac comes with PHP 5.3.26 but you will need at least PHP 5.5
	   Type [php -v] to check the version, if you already have PHP 5.5
	   then skip to #10
	5. So you will need to update, you can do this with the command
	   [curl -s http://php-osx.liip.ch/install.sh | bash -s 5.5]
	6. Now you update your bash profile, it should be in your home directory
	   You can get there with just [cd] I think
	7. open up the file .bash_profile in a text editor
	8. add the following line 
	   export PATH=~/bin:/usr/local/php5/bin:$PATH
	   You may already have a export line, if you do
	   add 
	   :/usr/local/php5/bin
	   just before the $PATH
	9.run [source.bash_profile]
	  then check with [php -v] to make sure you have the updated PHP version
	10. Run [php -S localhost:8000] to start your local server
	11. You can now view the files through the browser, go to
		"localhost:8000/index.php" on your browser to view the site
	 	

