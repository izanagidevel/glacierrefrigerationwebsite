Information(Text):



	Blurb for Main Page: 

ABOUT US
Glacier Refrigeration is a family owned and operated company.  We have been providing superior service to Calgary and area since 1994.
We pride ourselves on our knowledge in all areas of the commercial kitchen but our expertise extends beyond that.  We service, repair, replace, and maintain most commercial equipment.  We offer services for heating, ventilation, air conditioning, refrigeration and commercial equipment.
Our dedication to great service means that learning never ends.  We are continually upgrading and updating our technicians on the latest equipment, technologies and products in the industry.
Give us a call to get more detail on how we can help your business.



	Blurb for Services Page:

EQUIPMENT WE SERVICE

Refrigeration:
-Walk-in coolers
-Walk-in freezers
-Line coolers
-Ice machines
-Reach in coolers
-Display cases
-And much more

Heating and Air Conditioning:
-Ventilation
-Make-up air
-Exhaust fans

Food Service Equipment:
-Steam tables
-Heat lamps
-Mixers
-Ranges
-Fryers
-Ovens
-Convection ovens
-Proofers
-And much more

Preventative Maintenance Program:
Ask about our maintenance programs that are offered.  They are an important part of commercial equipment and can detect problem areas, reduce power consumption, and lengthen the life of your equipment.



	Blurb for Contact page: 

Our regular hours of service are:
Monday � Friday 8:00 am � 5:00 pm
We do offer 24 hour emergency service, which includes weekends and holidays.

If you have any questions or concerns, please feel free to leave us an email below, or phone us directly at 403-333-6909

***NOTE: HERE IS WHERE THE CONTACT BOX GOES** THE EMAIL ADDRESS IS DIFFERENT THAN PREVIOUSLY STATED***



	Blurb for Price Page:

Regular hourly rate: $95.00/hour
After hours rate: $142.50/hour
Weekends and Holidays rate: $190.00/hour
Truck charge: $95.00

Methods of payment:
-Cash
-Credit card
-E-transfer
-Cheque
*Due upon completion of job* <-- in italics please?



	Blurb for FAQ Page:

Q: If I don�t see the equipment that I need work done on, how do I know if you can help me?
A: Give us a call; there are too many things that we work on to list.

Q: How often do you suggest is the best for preventative maintenance?
A: We have found that every three months is the most common, but we assess your situation on an individual basis.

Q: What does your preventative maintenance service offer me?
A: Every customer is different in their needs and we assess your personal situation before we do maintenance.  Our technician will walk through with you to make a list of your equipment and what each piece will require and will explain to you what it is and why it is done.



*NOTE: MY DAD HAS ALSO REQUESTED A GALLERY PAGE THAT HE CAN ADD TO AND UPDATE LETTER ON WITH BEFORE AND AFTER PHOTOS**
	*NOTE, EMAIL HAS CHANGED*** Contact Email: glacierrefrigeration@shaw.ca
	Contact Phone Number: 403-333-6909
Images:
	Logo: In the "To Be Used" folder
	Background Image: In the "To Be Used" folder

Styling Decisions:

********Main Font: Big Caslon Medium********
NOTE: THE LOGO FILE SIZE IS 640x300 px.

	Background Color: #E3F8FF
	Navigation Bar Font (Color): #002D35
	Navigation Bar Background Color: #E3F8FF
	Navigation Bar Border:#004B4C
	Blurb Fonts (Color): #000000
	Blurb Background Colors: #E3F8FF
	Blurb Borders: #004B4C
	Footer Fonts (Color): #E3F8FF
	Footer Color: #002D35    